defmodule BritishMoney do
  @pennies_per_scilling 12
  @scillings_per_pound 20
  @pennies_per_pound @pennies_per_scilling * @scillings_per_pound

  defstruct [:total_pennies]

  def parts_of(m = %BritishMoney{}) do
    pounds = div(m.total_pennies, @pennies_per_pound)
    scillings = div(rem(m.total_pennies, @pennies_per_pound ), @pennies_per_scilling)
    pennies = rem(m.total_pennies, @pennies_per_scilling)
    {pounds, scillings, pennies}
  end

  def new(pounds, shillings, pennies) do
    %BritishMoney{total_pennies: pounds * @pennies_per_pound + shillings * @pennies_per_scilling + pennies}
  end

  def add(x = %BritishMoney{}, y = %BritishMoney{}) do
    %BritishMoney{total_pennies: x.total_pennies + y.total_pennies}
  end
end
