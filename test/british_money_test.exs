defmodule BritishMoneyTest do
  use ExUnit.Case

  test "sum BritishMoneys" do
    m0 = BritishMoney.new(1, 15, 11)
    m1 = BritishMoney.new(1, 15, 11)
    assert BritishMoney.add(m0, m1) == BritishMoney.new(3, 11, 10)
  end

  test "get BritishMoneys' parts" do
    m0 = BritishMoney.new(1, 15, 11)
    
    m1 = BritishMoney.new(1, 15, 11)

    assert BritishMoney.parts_of(BritishMoney.add(m0, m1)) == {3, 11, 10}
  end
end
